<?php get_header() ?>
<?php $link_options = get_option ( 'queens_theme_link_options' ); ?>
<?php $homepage_content = get_option ( 'queens_theme_homepage_content' ); ?>

	<div class="wrapper" itemscope itemtype="http://schema.org/Restaurant">
		<meta itemprop="name" content="<?php bloginfo('name') ?>">
		<meta itemprop="image" content="<?php the_field('slide_1') ?>">
		<section class="top">
			<div class="inner flex">
				<div><p><a href="<?php echo esc_url( $link_options['booking'] ) ?>">Book a table</a></p></div>
				<div class="social"><ul class="flex">
					<li><a href="<?php echo esc_url( $link_options['facebook'] ) ?>"><img src="<?php echo get_template_directory_uri() ?>/assets/img/fb_pink.svg" alt=""></a></li>
					<li><a href="<?php echo esc_url( $link_options['twitter'] ) ?>"><img src="<?php echo get_template_directory_uri() ?>/assets/img/twit_pink.svg" alt=""></a></li>
					<li><a href="<?php echo esc_url( $link_options['instagram'] ) ?>"><img src="<?php echo get_template_directory_uri() ?>/assets/img/insta_pink.svg" alt=""></a></li>
				</ul></div>
			</div>
		</section>
		<section class="main">
			<div class="inner">
				<div class="header">
					<p><?php echo $homepage_content['intro_line_1'] ?><br> <?php echo $homepage_content['intro_line_2'] ?></p>
					<h1><img src="<?php echo get_template_directory_uri() ?>/assets/img/logo_text.svg" alt="Queens Liverpool"></h1>
					<h1 class="mob"><img src="<?php echo get_template_directory_uri() ?>/assets/img/logo_mob.svg" alt="Queens"></h1>
					<p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span itemprop="streetAddress"><?php echo $homepage_content['address_line_1'] ?></span> <br><span itemprop="addressLocality"><?php echo $homepage_content['address_line_2'] ?></span></p>
				</div>
				<div class="slider_wrap">
					<div class="slider">
						<div class="slide">
							<div class="slide_inner">
								<figure class="_66" style="background-image:url('<?php the_field('slide_1') ?>');"></figure>
								<figure class="_33" style="background-image:url('<?php the_field('slide_2') ?>');"></figure>
							</div>
						</div>
						
						<div class="slide">
							<div class="slide_inner">
								<figure class="_33" style="background-image:url('<?php the_field('slide_3') ?>');"></figure>
								<figure class="_66" style="background-image:url('<?php the_field('slide_4') ?>');"></figure>
							</div>
						</div>
					</div>
					<div class="mob">
						<div class="slide_inner">
							<figure class="" style="background-image:url('<?php echo get_template_directory_uri() ?>/assets/img/slide1.png');"></figure>
							<figure class="_50" style="background-image:url('<?php echo get_template_directory_uri() ?>/assets/img/slide2.png');"></figure>
							<figure class="_50" style="background-image:url('<?php echo get_template_directory_uri() ?>/assets/img/slide3.png');"></figure>
							<figure class="" style="background-image:url('<?php echo get_template_directory_uri() ?>/assets/img/slide4.png');"></figure>
						</div>
					</div>
				</div>
				<div class="content">
					<div class="flex">
						<div class="logo"><img src="<?php echo get_template_directory_uri() ?>/assets/img/logo_icon.svg" alt=""></div>
						<div class="text"><p><?php echo $homepage_content['main_content'] ?></p></div>
						<div class="logo"><img src="<?php echo get_template_directory_uri() ?>/assets/img/logo_icon.svg" alt=""></div>
					</div>
				</div>
			</div>
		</section>
		<section class="info">
			<div class="inner">
				<h3>Opening Hours</h3>
				<p>MONDAY 
				<?php if ($homepage_content['hours_monday_start'] !== '') : ?>
					
					<meta itemprop="openingHours" content="Mo <?php echo $homepage_content['hours_monday_start'] ?>-<?php echo $homepage_content['hours_monday_end'] ?>">	
					<?php echo $homepage_content['hours_monday_start'] ?> – <?php echo $homepage_content['hours_monday_end'] ?></br>

				<? else : ?>
				<meta itemprop="openingHours" content="Mo closed">	
				Closed</br>	

				<? endif; ?>

				tuesday 
				<?php if ($homepage_content['hours_tuesday_start'] !== '') : ?>
					
					<meta itemprop="openingHours" content="Tu <?php echo $homepage_content['hours_tuesday_start'] ?>-<?php echo $homepage_content['hours_tuesday_end'] ?>">	
					<?php echo $homepage_content['hours_tuesday_start'] ?> – <?php echo $homepage_content['hours_tuesday_end'] ?></br>

				<? else : ?>
				<meta itemprop="openingHours" content="Tu closed">	
				Closed</br>	

				<? endif; ?>

				wednesday 
				<?php if ($homepage_content['hours_wednesday_start'] !== '') : ?>
					
					<meta itemprop="openingHours" content="Wed <?php echo $homepage_content['hours_wednesday_start'] ?>-<?php echo $homepage_content['hours_wednesday_end'] ?>">	
					<?php echo $homepage_content['hours_wednesday_start'] ?> – <?php echo $homepage_content['hours_wednesday_end'] ?></br>

				<? else : ?>
				<meta itemprop="openingHours" content="Wed closed">	
				Closed</br>	

				<? endif; ?>

				thursday 
				<?php if ($homepage_content['hours_thursday_start'] !== '') : ?>
					
					<meta itemprop="openingHours" content="Th <?php echo $homepage_content['hours_thursday_start'] ?>-<?php echo $homepage_content['hours_thursday_end'] ?>">	
					<?php echo $homepage_content['hours_thursday_start'] ?> – <?php echo $homepage_content['hours_thursday_end'] ?></br>

				<? else : ?>
				<meta itemprop="openingHours" content="Th closed">	
				Closed</br>	

				<? endif; ?>

				friday 
				<?php if ($homepage_content['hours_friday_start'] !== '') : ?>
					
					<meta itemprop="openingHours" content="Fr <?php echo $homepage_content['hours_friday_start'] ?>-<?php echo $homepage_content['hours_friday_end'] ?>">	
					<?php echo $homepage_content['hours_friday_start'] ?> – <?php echo $homepage_content['hours_friday_end'] ?></br>

				<? else : ?>
				<meta itemprop="openingHours" content="Fr closed">	
				Closed</br>	

				<? endif; ?>

				saturday 
				<?php if ($homepage_content['hours_saturday_start'] !== '') : ?>
					
					<meta itemprop="openingHours" content="Sa <?php echo $homepage_content['hours_saturday_start'] ?>-<?php echo $homepage_content['hours_saturday_end'] ?>">	
					<?php echo $homepage_content['hours_saturday_start'] ?> – <?php echo $homepage_content['hours_saturday_end'] ?></br>

				<? else : ?>
				<meta itemprop="openingHours" content="Sa closed">	
				Closed</br>	

				<? endif; ?>

				sunday 
				<?php if ($homepage_content['hours_sunday_start'] !== '') : ?>
					
					<meta itemprop="openingHours" content="Su <?php echo $homepage_content['hours_sunday_start'] ?>-<?php echo $homepage_content['hours_sunday_end'] ?>">	
					<?php echo $homepage_content['hours_sunday_start'] ?> – <?php echo $homepage_content['hours_sunday_end'] ?></br>

				<? else : ?>
				<meta itemprop="openingHours" content="Su closed">	
				Closed</p>	

				<? endif; ?>

				
				<h3>CONTACT</h3>

				<p><a href="mailto:<?php echo $homepage_content['contact_email'] ?>"><?php echo $homepage_content['contact_email'] ?></a>
				<br><a href="tel:<?php echo $homepage_content['contact_phone'] ?>" itemprop="telephone" content="<?php echo $homepage_content['contact_phone'] ?>"><?php echo $homepage_content['contact_phone'] ?></a></p>
			</div>
		</section>
		<section class="footer">
			<div class="inner">
				<ul class="social flex">
					<li><a href="<?php echo esc_url( $link_options['facebook'] ) ?>"><img src="<?php echo get_template_directory_uri() ?>/assets/img/fb_green.svg" alt=""></a></li>
					<li><a href="<?php echo esc_url( $link_options['twitter'] ) ?>"><img src="<?php echo get_template_directory_uri() ?>/assets/img/twit_green.svg" alt=""></a></li>
					<li><a href="<?php echo esc_url( $link_options['instagram'] ) ?>"><img src="<?php echo get_template_directory_uri() ?>/assets/img/insta_green.svg" alt=""></a></li>
				</ul>
				<p class="address"><?php echo $homepage_content['address_line_1'] ?> <br><?php echo $homepage_content['address_line_2'] ?>
				</p>
			</div>
		</section>
	</div>

	<?php get_footer() ?>