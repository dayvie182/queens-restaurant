<?php

/**
 * This function introduces the theme options into the 'Appearance' menu and into a top-level 
 * 'Queens Theme' menu.
 */
function queens_theme_menu() {

	add_theme_page(
		'Queens Theme', 					// The title to be displayed in the browser window for this page.
		'Queens Theme',					// The text to be displayed for this menu item
		'administrator',					// Which type of users can see this menu item
		'queens_theme_options',			// The unique ID - that is, the slug - for this menu item
		'queens_theme_display'				// The name of the function to call when rendering this menu's page
	);
	
	add_menu_page(
		'Queens Theme',					// The value used to populate the browser's title bar when the menu page is active
		'Queens Theme',					// The text of the menu in the administrator's sidebar
		'administrator',					// What roles are able to access the menu
		'queens_theme_menu',				// The ID used to bind submenu items to this menu 
		'queens_theme_display'				// The callback function used to render this menu
	);
	
	add_submenu_page(
		'queens_theme_menu',
		__( 'Social & Link Options', 'queens' ),
		__( 'Social & Link Options', 'queens' ),
		'administrator',
		'queens_theme_link_options',
		create_function( null, 'queens_theme_display( "link_options" );' )
	);
	
	add_submenu_page(
		'queens_theme_menu',
		__( 'Website Info', 'queens' ),
		__( 'Website Info', 'queens' ),
		'administrator',
		'queens_theme_homepage_content',
		create_function( null, 'queens_theme_display( "homepage_content" );' )
	);


} // end queens_theme_menu
add_action( 'admin_menu', 'queens_theme_menu' );

/**
 * Renders a simple page to display for the theme menu defined above.
 */
function queens_theme_display( $active_tab = '' ) {
?>
	<!-- Create a header in the default WordPress 'wrap' container -->
	<div class="wrap">
	
		<div id="icon-themes" class="icon32"></div>
		<h2><?php _e( 'Queens Theme Options', 'queens' ); ?></h2>
		<?php settings_errors(); ?>
		
		<?php if( isset( $_GET[ 'tab' ] ) ) {
			$active_tab = $_GET[ 'tab' ];
		} else if( $active_tab == 'link_options' ) {
			$active_tab = 'link_options';
		} else {
			$active_tab = 'homepage_content';
		} // end if/else ?>
		
		<h2 class="nav-tab-wrapper">
			<a href="?page=queens_theme_options&tab=link_options" class="nav-tab <?php echo $active_tab == 'link_options' ? 'nav-tab-active' : ''; ?>"><?php _e( 'Social & Link Options', 'queens' ); ?></a>
			<a href="?page=queens_theme_options&tab=homepage_content" class="nav-tab <?php echo $active_tab == 'homepage_content' ? 'nav-tab-active' : ''; ?>"><?php _e( 'Website Info', 'queens' ); ?></a>
		</h2>
		
		<form method="post" action="options.php">
			<?php
			
				if( $active_tab == 'link_options' ) {
				
					settings_fields( 'queens_theme_link_options' );
					do_settings_sections( 'queens_theme_link_options' );
					
				} else {
				
					settings_fields( 'queens_theme_homepage_content' );
					do_settings_sections( 'queens_theme_homepage_content' );
					
				} // end if/else
				
				submit_button();
			
			?>
		</form>
		
	</div><!-- /.wrap -->
<?php
} // end queens_theme_display

/* ------------------------------------------------------------------------ *
 * Setting Registration
 * ------------------------------------------------------------------------ */ 


/**
 * Provides default values for the Social & Link Options.
 */
function queens_theme_default_link_options() {
	
	$defaults = array(
		'twitter'		=>	'',
		'facebook'		=>	'',
		'instagram'	=>	'',
		'booking'	=>	'',
	);
	
	return apply_filters( 'queens_theme_default_link_options', $defaults );
	
} // end queens_theme_default_link_options

/**
 * Provides default values for the Input Options.
 */
function queens_theme_homepage_content_options() {
	
	$defaults = array(
		'intro_line_1' =>	'',
		'intro_line_2'	=>	'',
		'main_content'	=>	'',
		'contact_phone'	=>	'',
		'contact_email'	=>	'',
		'address_line_1' =>	'',
		'address_line_2' =>	'',
		'hours_monday_start'	=>	'',
		'hours_monday_end'	=>	'',
		'hours_tuesday_start'	=>	'',
		'hours_tuesday_end'	=>	'',
		'hours_wednesday_start'	=>	'',
		'hours_wednesday_end'	=>	'',
		'hours_thursday_start'	=>	'',
		'hours_thursday_end'	=>	'',
		'hours_friday_start'	=>	'',
		'hours_friday_end'	=>	'',
		'hours_saturday_start'	=>	'',
		'hours_saturday_end'	=>	'',
		'hours_sunday_start'	=>	'',
		'hours_sunday_end'	=>	'',

		
	);
	
	return apply_filters( 'queens_theme_homepage_content_options', $defaults );
	
} // end queens_theme_homepage_content_options


/**
 * Initializes the theme's social & Link options by registering the Sections,
 * Fields, and Settings.
 *
 * This function is registered with the 'admin_init' hook.
 */ 
function queens_theme_initialize_link_options() {

	if( false == get_option( 'queens_theme_link_options' ) ) {	
		add_option( 'queens_theme_link_options', apply_filters( 'queens_theme_default_link_options', queens_theme_default_link_options() ) );
	} // end if
	
	add_settings_section(
		'link_settings_section',			// ID used to identify this section and with which to register options
		__( 'Social & Link Options', 'queens' ),		// Title to be displayed on the administration page
		'queens_link_options_callback',	// Callback used to render the description of the section
		'queens_theme_link_options'		// Page on which to add this section of options
	);
	
	add_settings_field(	
		'twitter',						
		'Twitter',							
		'queens_twitter_callback',	
		'queens_theme_link_options',	
		'link_settings_section'			
	);

	add_settings_field(	
		'facebook',						
		'Facebook',							
		'queens_facebook_callback',	
		'queens_theme_link_options',	
		'link_settings_section'			
	);
	
	add_settings_field(	
		'instagram',						
		'Instagram',							
		'queens_instagram_callback',	
		'queens_theme_link_options',	
		'link_settings_section'			
	);

	add_settings_field(	
		'booking',						
		'\'Book a Table\' link',							
		'queens_booking_callback',	
		'queens_theme_link_options',	
		'link_settings_section'			
	);
	
	register_setting(
		'queens_theme_link_options',
		'queens_theme_link_options',
		'queens_theme_sanitize_link_options'
	);
	
} // end queens_theme_initialize_link_options
add_action( 'admin_init', 'queens_theme_initialize_link_options' );

/**
 * Initializes the theme's input example by registering the Sections,
 * Fields, and Settings. This particular group of options is used to demonstration
 * validation and sanitization.
 *
 * This function is registered with the 'admin_init' hook.
 */ 
function queens_theme_initialize_homepage_content() {

	if( false == get_option( 'queens_theme_homepage_content' ) ) {	
		add_option( 'queens_theme_homepage_content', apply_filters( 'queens_theme_homepage_content_options', queens_theme_homepage_content_options() ) );
	} // end if

	add_settings_section(
		'homepage_content_section',
		__( 'Website Info', 'queens' ),
		'queens_homepage_content_callback',
		'queens_theme_homepage_content'
	);

	add_settings_field(
		'intro_line',
		__('Intro text', 'queens'),
		'queens_intro_text_callback',
		'queens_theme_homepage_content',
		'homepage_content_section'
	);
	
	add_settings_field(
		'main_content',
		__('Main Content', 'queens'),
		'queens_main_content_callback',
		'queens_theme_homepage_content',
		'homepage_content_section'
	);
	add_settings_field(
		'contact_phone',
		__('Phone number', 'queens'),
		'queens_contact_phone_callback',
		'queens_theme_homepage_content',
		'homepage_content_section'
	);
	add_settings_field(
		'contact_email',
		__('Email address', 'queens'),
		'queens_contact_email_callback',
		'queens_theme_homepage_content',
		'homepage_content_section'
	);
	add_settings_field(
		'address',
		__('Address', 'queens'),
		'queens_address_callback',
		'queens_theme_homepage_content',
		'homepage_content_section'
	);
	
	add_settings_field(
		'opening_hours',
		__('Opening hours', 'queens'),
		'queens_hours_callback',
		'queens_theme_homepage_content',
		'homepage_content_section'
	);
	
	
	register_setting(
		'queens_theme_homepage_content',
		'queens_theme_homepage_content',
		'queens_theme_validate_homepage_content'
	);

} // end queens_theme_initialize_homepage_content
add_action( 'admin_init', 'queens_theme_initialize_homepage_content' );

/* ------------------------------------------------------------------------ *
 * Section Callbacks
 * ------------------------------------------------------------------------ */ 


/**
 * This function provides a simple description for the Social & Link Options page. 
 *
 * It's called from the 'queens_theme_initialize_link_options' function by being passed as a parameter
 * in the add_settings_section function.
 */
function queens_link_options_callback() {
	echo '<p>' . __( 'Provide links to the social icons and Book Table button.', 'queens' ) . '</p>';
} // end queens_general_options_callback

/**
 * This function provides a simple description for the Website Info page.
 *
 * It's called from the 'queens_theme_initialize_homepage_content_options' function by being passed as a parameter
 * in the add_settings_section function.
 */
function queens_homepage_content_callback() {
	echo '<p>' . __( 'Update content on the homepage', 'queens' ) . '</p>';
} // end queens_general_options_callback

/* ------------------------------------------------------------------------ *
 * Field Callbacks
 * ------------------------------------------------------------------------ */ 

/**
 * This function renders the interface elements for toggling the visibility of the header element.
 * 
 * It accepts an array or arguments and expects the first element in the array to be the description
 * to be displayed next to the checkbox.
 */


function queens_twitter_callback() {
	
	// First, we read the social & Link options collection
	$options = get_option( 'queens_theme_link_options' );
	
	// Next, we need to make sure the element is defined in the options. If not, we'll set an empty string.
	$url = '';
	if( isset( $options['twitter'] ) ) {
		$url = esc_url( $options['twitter'] );
	} // end if
	
	// Render the output
	echo '<input type="text" id="twitter" name="queens_theme_link_options[twitter]" value="' . $url . '" />';
	
} // end queens_twitter_callback

function queens_booking_callback() {
	
	// First, we read the social & Link options collection
	$options = get_option( 'queens_theme_link_options' );
	
	// Next, we need to make sure the element is defined in the options. If not, we'll set an empty string.
	$url = '';
	if( isset( $options['booking'] ) ) {
		$url = esc_url( $options['booking'] );
	} // end if
	
	// Render the output
	echo '<input type="text" id="booking" name="queens_theme_link_options[booking]" value="' . $url . '" />';
	
} // end queens_booking_callback

function queens_facebook_callback() {
	
	$options = get_option( 'queens_theme_link_options' );
	
	$url = '';
	if( isset( $options['facebook'] ) ) {
		$url = esc_url( $options['facebook'] );
	} // end if
	
	// Render the output
	echo '<input type="text" id="facebook" name="queens_theme_link_options[facebook]" value="' . $url . '" />';
	
} // end queens_facebook_callback

function queens_instagram_callback() {
	
	$options = get_option( 'queens_theme_link_options' );
	
	$url = '';
	if( isset( $options['instagram'] ) ) {
		$url = esc_url( $options['instagram'] );
	} // end if
	
	// Render the output
	echo '<input type="text" id="instagram" name="queens_theme_link_options[instagram]" value="' . $url . '" />';
	
} // end queens_instagram_callback

function queens_intro_text_callback() {
	$options = get_option('queens_theme_homepage_content');
	echo '<input type="text" id="intro_line_1" name="queens_theme_homepage_content[intro_line_1]" value="' . $options['intro_line_1'] . '" /><br>';
	echo '<input type="text" id="intro_line_2" name="queens_theme_homepage_content[intro_line_2]" value="' . $options['intro_line_2'] . '" />';
}

function queens_main_content_callback() {
	$options = get_option('queens_theme_homepage_content');
	echo '<textarea id="main_content" name="queens_theme_homepage_content[main_content]" rows="5" cols="50">' . $options['main_content'] . '</textarea>';
}
function queens_contact_phone_callback() {
	$options = get_option('queens_theme_homepage_content');
	echo '<input type="text" id="contact_phone" name="queens_theme_homepage_content[contact_phone]" value="' . $options['contact_phone'] . '" />';
}
function queens_contact_email_callback() {
	$options = get_option('queens_theme_homepage_content');
	echo '<input type="text" id="contact_email" name="queens_theme_homepage_content[contact_email]" value="' . $options['contact_email'] . '" />';
}
function queens_address_callback() {
	$options = get_option('queens_theme_homepage_content');
	echo '<input type="text" id="address_line_1" name="queens_theme_homepage_content[address_line_1]" value="' . $options['address_line_1'] . '" /><br>';
	echo '<input type="text" id="address_line_2" name="queens_theme_homepage_content[address_line_2]" value="' . $options['address_line_2'] . '" />';
}

function queens_hours_callback() {
	$options = get_option('queens_theme_homepage_content');
	echo "<ul>";
	echo "<li><label>Monday: </label>";
	echo '<input type="text" id="hours_monday_start" name="queens_theme_homepage_content[hours_monday_start]" value="' . $options['hours_monday_start'] . '" /> - ';
	echo '<input type="text" id="hours_monday_end" name="queens_theme_homepage_content[hours_monday_end]" value="' . $options['hours_monday_end'] . '" /></li>';

	echo "<li><label>tuesday: </label>";
	echo '<input type="text" id="hours_tuesday_start" name="queens_theme_homepage_content[hours_tuesday_start]" value="' . $options['hours_tuesday_start'] . '" /> - ';
	echo '<input type="text" id="hours_tuesday_end" name="queens_theme_homepage_content[hours_tuesday_end]" value="' . $options['hours_tuesday_end'] . '" /></li>';

	echo "<li><label>wednesday: </label>";
	echo '<input type="text" id="hours_wednesday_start" name="queens_theme_homepage_content[hours_wednesday_start]" value="' . $options['hours_wednesday_start'] . '" /> - ';
	echo '<input type="text" id="hours_wednesday_end" name="queens_theme_homepage_content[hours_wednesday_end]" value="' . $options['hours_wednesday_end'] . '" /></li>';

	echo "<li><label>thursday: </label>";
	echo '<input type="text" id="hours_thursday_start" name="queens_theme_homepage_content[hours_thursday_start]" value="' . $options['hours_thursday_start'] . '" /> - ';
	echo '<input type="text" id="hours_thursday_end" name="queens_theme_homepage_content[hours_thursday_end]" value="' . $options['hours_thursday_end'] . '" /></li>';

	echo "<li><label>friday: </label>";
	echo '<input type="text" id="hours_friday_start" name="queens_theme_homepage_content[hours_friday_start]" value="' . $options['hours_friday_start'] . '" /> - ';
	echo '<input type="text" id="hours_friday_end" name="queens_theme_homepage_content[hours_friday_end]" value="' . $options['hours_friday_end'] . '" /></li>';

	echo "<li><label>saturday: </label>";
	echo '<input type="text" id="hours_saturday_start" name="queens_theme_homepage_content[hours_saturday_start]" value="' . $options['hours_saturday_start'] . '" /> - ';
	echo '<input type="text" id="hours_saturday_end" name="queens_theme_homepage_content[hours_saturday_end]" value="' . $options['hours_saturday_end'] . '" /></li>';

	echo "<li><label>sunday: </label>";
	echo '<input type="text" id="hours_sunday_start" name="queens_theme_homepage_content[hours_sunday_start]" value="' . $options['hours_sunday_start'] . '" /> - ';
	echo '<input type="text" id="hours_sunday_end" name="queens_theme_homepage_content[hours_sunday_end]" value="' . $options['hours_sunday_end'] . '" /></li>';

	echo '</ul>';
}

function queens_input_element_callback() {
	
	$options = get_option( 'queens_theme_homepage_content' );
	
	// Render the output
	echo '<input type="text" id="input_example" name="queens_theme_homepage_content[input_example]" value="' . $options['input_example'] . '" />';
	
} // end queens_input_element_callback

function queens_textarea_element_callback() {
	
	$options = get_option( 'queens_theme_homepage_content' );
	
	// Render the output
	echo '<textarea id="textarea_example" name="queens_theme_homepage_content[textarea_example]" rows="5" cols="50">' . $options['textarea_example'] . '</textarea>';
	
} // end queens_textarea_element_callback

function queens_checkbox_element_callback() {

	$options = get_option( 'queens_theme_homepage_content' );
	
	$html = '<input type="checkbox" id="checkbox_example" name="queens_theme_homepage_content[checkbox_example]" value="1"' . checked( 1, $options['checkbox_example'], false ) . '/>';
	$html .= '&nbsp;';
	$html .= '<label for="checkbox_example">This is an example of a checkbox</label>';
	
	echo $html;

} // end queens_checkbox_element_callback

function queens_radio_element_callback() {

	$options = get_option( 'queens_theme_homepage_content' );
	
	$html = '<input type="radio" id="radio_example_one" name="queens_theme_homepage_content[radio_example]" value="1"' . checked( 1, $options['radio_example'], false ) . '/>';
	$html .= '&nbsp;';
	$html .= '<label for="radio_example_one">Option One</label>';
	$html .= '&nbsp;';
	$html .= '<input type="radio" id="radio_example_two" name="queens_theme_homepage_content[radio_example]" value="2"' . checked( 2, $options['radio_example'], false ) . '/>';
	$html .= '&nbsp;';
	$html .= '<label for="radio_example_two">Option Two</label>';
	
	echo $html;

} // end queens_radio_element_callback

function queens_select_element_callback() {

	$options = get_option( 'queens_theme_homepage_content' );
	
	$html = '<select id="time_options" name="queens_theme_homepage_content[time_options]">';
		$html .= '<option value="default">' . __( 'Select a time option...', 'queens' ) . '</option>';
		$html .= '<option value="never"' . selected( $options['time_options'], 'never', false) . '>' . __( 'Never', 'queens' ) . '</option>';
		$html .= '<option value="sometimes"' . selected( $options['time_options'], 'sometimes', false) . '>' . __( 'Sometimes', 'queens' ) . '</option>';
		$html .= '<option value="always"' . selected( $options['time_options'], 'always', false) . '>' . __( 'Always', 'queens' ) . '</option>';	$html .= '</select>';
	
	echo $html;

} // end queens_radio_element_callback

/* ------------------------------------------------------------------------ *
 * Setting Callbacks
 * ------------------------------------------------------------------------ */ 
 
/**
 * Sanitization callback for the social & Link options. Since each of the social & Link options are text inputs,
 * this function loops through the incoming option and strips all tags and slashes from the value
 * before serializing it.
 *	
 * @params	$input	The unsanitized collection of options.
 *
 * @returns			The collection of sanitized values.
 */
function queens_theme_sanitize_link_options( $input ) {
	
	// Define the array for the updated options
	$output = array();

	// Loop through each of the options sanitizing the data
	foreach( $input as $key => $val ) {
	
		if( isset ( $input[$key] ) ) {
			$output[$key] = esc_url_raw( strip_tags( stripslashes( $input[$key] ) ) );
		} // end if	
	
	} // end foreach
	
	// Return the new collection
	return apply_filters( 'queens_theme_sanitize_link_options', $output, $input );

} // end queens_theme_sanitize_link_options

function queens_theme_validate_homepage_content( $input ) {

	// Create our array for storing the validated options
	$output = array();
	
	// Loop through each of the incoming options
	foreach( $input as $key => $value ) {
		
		// Check to see if the current option has a value. If so, process it.
		if( isset( $input[$key] ) ) {
		
			// Strip all HTML and PHP tags and properly handle quoted strings
			$output[$key] = strip_tags( stripslashes( $input[ $key ] ) );
			
		} // end if
		
	} // end foreach
	
	// Return the array processing any additional functions filtered by this action
	return apply_filters( 'queens_theme_validate_homepage_content', $output, $input );

} // end queens_theme_validate_homepage_content


// detect browser type
function wp_browser_body_class($classes) {
global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;
if($is_lynx) $classes[] = 'lynx';
elseif($is_gecko) $classes[] = 'gecko';
elseif($is_opera) $classes[] = 'opera';
elseif($is_NS4) $classes[] = 'ns4';
elseif($is_safari) $classes[] = 'safari';
elseif($is_chrome) $classes[] = 'chrome';
elseif($is_IE) {
$classes[] = 'ie';
if(preg_match('/MSIE ([0-9]+)([a-zA-Z0-9.]+)/', $_SERVER['HTTP_USER_AGENT'], $browser_version))
$classes[] = 'ie'.$browser_version[1];
} else $classes[] = 'unknown';
if($is_iphone) $classes[] = 'iphone';
if ( stristr( $_SERVER['HTTP_USER_AGENT'],"mac") ) {
$classes[] = 'osx';
} elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"linux") ) {
$classes[] = 'linux';
} elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"windows") ) {
$classes[] = 'windows';
}
return $classes;
}
add_filter('body_class','wp_browser_body_class');



?>
