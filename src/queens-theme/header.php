<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
 	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- stylesheets -->
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/style.css">

	<!-- twitter card data -->
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="<?php bloginfo('name') ?>">
	<meta name="twitter:url" content="<?php bloginfo('url') ?>">
	<meta name="twitter:title" content="<?php bloginfo('name') ?>">
	<meta name="twitter:description" content="<?php echo get_option ( 'queens_theme_homepage_content' )['main_content'] ?>">

	<!-- open graph data -->
	<meta property="og:title" content="<?php bloginfo('name') ?>">
	<meta property="og:description" content="<?php echo get_option ( 'queens_theme_homepage_content' )['main_content'] ?>">
	<meta property="og:type" content="website">
	<meta property="og:url" content="<?php bloginfo('url') ?>">
	<meta property="og:image" content="<?php the_field('slide_1') ?>">

	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri() ?>/assets/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri() ?>/assets/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri() ?>/assets/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri() ?>/assets/favicon/site.webmanifest">
	<link rel="mask-icon" href="<?php echo get_template_directory_uri() ?>/assets/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri() ?>/assets/favicon/favicon.ico">
	<meta name="msapplication-TileColor" content="#e8e2d9">
	<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri() ?>/assets/favicon/mstile-144x144.png">
	<meta name="msapplication-config" content="<?php echo get_template_directory_uri() ?>/assets/favicon/browserconfig.xml">
	<meta name="theme-color" content="#e8e2d9">

	<title><?php wp_title() ?></title>
	<meta name="description" content="<?php echo get_option ( 'queens_theme_homepage_content' )['main_content'] ?>">
	
	<?php wp_head() ?>

</head>

<body <?php body_class()?> >